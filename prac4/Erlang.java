public class Erlang
{
    private static int cnt;

    public static void main (String[] args)
    {
	int N = Integer.parseInt(args[0]);
	int p = Integer.parseInt(args[1]);
	double E = Erlangrec(N,p);
	double En = Erlangnrec(N,p);
	System.out.println(E);
	System.out.println(En);
	System.out.println(cnt);
    }

    private static int fact (int n)
    {
	if (n <= 1)
	    return  1;
	else
	    return  n * fact(n - 1);
    }

    private static double Erlangnrec (int N, int p)
    {
	double denum = 0;
	double num = (Math.pow(p,N))/(fact(N));
	for (int i=0; i<N; i++) {
	    denum = ((Math.pow(p,i)) / (fact(i))) + denum;
	}
	return num / denum;
    }

    private static double Erlangrec (int N, int p)
    {
	if (N == 0)
	    return 1;
	else {
	    cnt++;
	    return (p*Erlangrec(N-1,p)) / (N+p*Erlangrec(N-1,p));
	}
    }
}