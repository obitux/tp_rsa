import java.awt.Color;

public class Barnsley
{
    private static int N;
    private static double X;
    private static double Y;

    public static void main (String[] args)
    {
	N = Integer.parseInt(args[0]);
	X = 0.5;
	Y = 0.0;
	StdDraw.setPenColor(new Color(0,102,0));

	for (int i=0; i<N; i++) {
	    StdDraw.point(X,Y);
	    newPoint();
	}
	StdDraw.point(X,Y);
    }

    public static void newPoint ()
    {
	double last_X = X;
	double r = Math.random();
	
	if (r <= 0.02) {
	    X = 0.5;
	    Y *= 0.27;
	}
	else if (r <= 0.17) {
	    X = 0.57 + 0.263*Y - 0.139*X;
	    Y = 0.246*last_X + 0.224*Y - 0.036;
	}
	else if (r <= 0.30) {
	    X = 0.170*X - 0.215*Y + 0.408;
	    Y = 0.222*last_X + 0.176*Y + 0.0893;
	}
	else {
	    X = 0.781*X + 0.034*Y + 0.1075;
	    Y = 0.739*Y - 0.032*last_X + 0.27;
	}
    }
}