public class VotingMachines
{
    private static int VOTERS = 100000000;
    private static double ERROR_RATE = 0.8;
    private static int A;
    private static int B;

    public static void main (String[] args)
    {
	A = 0;
	B = 0;

	StdOut.print("Choose an error rate : ");
	ERROR_RATE = StdIn.readDouble();

	for (int i=0; i<VOTERS*0.51; i++) {
	    vote_A();
	}
	for (int i=0; i<VOTERS*0.49; i++) {
	    vote_B();
	}

	StdOut.println("Voters: " + (A+B));
	StdOut.println("Error rate: " + ERROR_RATE);
	StdOut.println("A: " + A + " | B: " + B);
    }

    public static void vote_A ()
    {
	double r = Math.random();
	if (r < ERROR_RATE)
	    B++;
	else
	    A++;
    }

    public static void vote_B ()
    {
	double r = Math.random();
	if (r < ERROR_RATE)
            A++;
	else
            B++;
    }
}