public class passwordVerification
{
    public static void main (String[] args)
    {
	System.out.println(checkPasswd(args[0]));
    }

    public static Boolean checkPasswd(String passwd)
    {
	Boolean length=false, digit=false, upper=false, lower=false, other=false;
	
	// lenght >=8 ?
	if (passwd.length() > 7)
	    length = true;

	// any digit?
	for (int i=0; i<passwd.length(); i++) {
	    if (passwd.charAt(i) < 58 && passwd.charAt(i) > 47) {
		digit = true;
		break;
	    } 
	}

	// any upper case letter?
	for (int i=0; i<passwd.length(); i++) {
            if (passwd.charAt(i) < 91 && passwd.charAt(i) > 64){
		upper = true;
		break;
            }
        }

	// any lower case letter?
	for (int i=0; i<passwd.length(); i++) {
            if (passwd.charAt(i) < 123 && passwd.charAt(i) > 96){
		lower = true;
		break;
            }
	}

	// any other char?
	for (int i=0; i<passwd.length(); i++) {
            if ( (passwd.charAt(i) < 48 && passwd.charAt(i) > 32) || 
		 (passwd.charAt(i) < 65 && passwd.charAt(i) > 57) ||
		 (passwd.charAt(i) < 97 && passwd.charAt(i) > 90) ||
		 (passwd.charAt(i) < 127 && passwd.charAt(i) > 123) )
		{
		    other = true;
		    break;
		}
	}

	return (length && digit && upper && lower && other);
    }
}