public class reverseDomain
{
    public static void main (String[] args)
    {
	String domain = args[0];
	String reversedDomain = "";
	String buffer = "";

	for (int i=domain.length() -1; i>=0; i--) {
	    if (domain.charAt(i) == '.') {
		buffer = reverseBuffer(buffer);
		reversedDomain += buffer;
		reversedDomain += '.';
		buffer = "";
	    }
	    else {
		buffer += domain.charAt(i);
	    }
	}
	reversedDomain += reverseBuffer(buffer);

	System.out.println(reversedDomain);
    }

    public static String reverseBuffer (String buffer)
    {
	String result = "";
	
	for (int i = buffer.length() -1; i >= 0; i--) {
	    result += buffer.charAt(i);
	}
	
	return result;
    }
}