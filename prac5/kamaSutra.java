public class kamaSutra
{
    public static void main(String[] args)
    {
	String keyA = args[0];
	String keyB = args[1];
	
	if (keyA.length() != keyB.length())
	    System.out.println("Keys have not the same length");

	else {
	    String text = "";
	    
	    while (!StdIn.isEmpty())
		text = StdIn.readString();
	    
	    for (int i = 0; i < text.length(); i++) {
		char x = text.charAt(i);
		
		if (keyA.indexOf(x) >= 0) {
		    int j = keyA.indexOf(x);
		    System.out.print(keyB.charAt(j));
		}
		
		else {
		    int k = keyB.indexOf(x);
		    System.out.print(keyA.charAt(k));
		}
	    }
	    
	    System.out.println("");
	}
    }
}