public class Minesweeper
{
    public static int X;
    public static int Y;
    public static char[][] Map;

    public static void main (String[] args)
    {
	load_map();
	print_map();
    }

    public static void load_map ()
    {
	Y = StdIn.readInt();
	X = StdIn.readInt();
        Map = new char[X][Y];
	Y--;
	X--;
	
	for (int y=0; y<=Y; y++) {
            for(int x=0; x<=X; x++) {
		StdIn.readChar();
		Map[x][y] = StdIn.readChar();
            }
        }
    }

    public static void print_map ()
    {
	for (int y=0; y<=Y; y++) {
	    for (int x=0; x<=X; x++) {
		StdOut.print(Map[x][y] + " ");
	    }
	    StdOut.print("\n");
	}
	
	StdOut.println();
	
	for (int y=0; y<=Y; y++) {
            for (int x=0; x<=X; x++) {
		int n = count_mines(x,y);
		if (n==-1)
		    StdOut.print("* ");
		else
		    StdOut.print(n + " ");
            }
            StdOut.print("\n");
	}
    }

    public static int count_mines (int x, int y)
    {
	if (Map[x][y] == '*')
	    return -1;

	int count = 0;

	for (int j=y-1; j<=y+1; j++) {
	    for (int i=x-1; i<=x+1; i++) {
		if (i>-1 && j>-1 && i<X+1 && j<Y+1 && Map[i][j] == '*') {
		    count++;
		}
	    }
	}
	
	return count;
    }
}