public class Question1 {

    /* A slow way to calculate the nth value of the mystery series */

    public static long mystery_series(int n) {
	
	if (n == 0) return 0;
	if (n == 1) return 1;
	if (n == 2) return 1;
	return mystery_series(n-1) + mystery_series(n-2) + mystery_series(n-3);
    }

    public static long mystery_series_2 (int n) {
	long[] results = new long[n+1];
	results[0] = 0;
	results[1] = 1;
	results[2] = 1;
	for (int i=3; i<=n; i++) {
	    results[i] = results[i-1] + results[i-2] + results[i-3];
	}
	return results[n];
    }
    
    public static void main(String[] args) {
	
	/* Input n and print out the nth value of the mystery series: */
	
	int n = Integer.parseInt(args[0]);
	StdOut.println("i: " + mystery_series_2(n));
	//StdOut.println("r: " + mystery_series(n));
    }
}