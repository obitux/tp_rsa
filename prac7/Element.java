import java.io.BufferedReader; 
import java.io.FileReader; 
import java.util.ArrayList; 
import java.util.Collections;
import java.util.HashMap; 

public class Element 
{
    public static HashMap<String, Integer> countries = new HashMap<String, Integer>();
    public static ArrayList<Integer> values = new ArrayList<Integer>();
    
    public static void main(String[] args) {

	read_countries();
	read_ips();
	values.addAll(countries.values());
	Collections.sort(values, Collections.reverseOrder());	
        
	int j = -1;
	for (Integer i : values) { 
	    if (j == i) 
                continue;
            j = i;
            for (String s : countries.keySet() ) { 
                if (countries.get(s) == i) 
                    System.out.println(" ~   " + s + " : " + i);
            }
	}
    }
	
    private static void read_countries() {
	
	try {
	    BufferedReader file = new BufferedReader(new FileReader("phone-international.srt"));
	    String s = file.readLine();
	    String id = file.readLine();
	    
	    while(!(id.charAt(0) == '2' && id.charAt(1) == '9' && id.charAt(2) == '7')) {
		if (id.charAt(0) != '2')	
		    id = file.readLine();
		else {
		    String[] ss = id.split(",");
		    countries.put(ss[1],0); 
		    id = file.readLine();
		}
	    }
	}
	
	catch(Exception e) {
	    e.printStackTrace();
	    System.exit(1);
	}
    }
    
    private static void read_ips() {
	
	try {
	    BufferedReader file = new BufferedReader(new FileReader("ip-by-country.srt"));
	    String ip;
	    int total = 0;
	    String last_country = "";
	    String s1 = file.readLine();
	    
	    while((ip = file.readLine()) != null) {

		String[] t_ip = ip.split(",");
		long diff_1 = Long.parseLong(t_ip[2]);
		long diff_2 = Long.parseLong(t_ip[3]);
		int min = (int)(diff_2 - diff_1);
		int current_val = min;
		
		if(countries.containsKey(t_ip[5])) {
		    if(last_country.equals(t_ip[5]))
			total = current_val + total;
		    else 
			total = current_val;
		    countries.put(t_ip[5],total);
		}
		last_country = t_ip[5];
	    }	
	}
	
	catch(Exception e) {
	    e.printStackTrace();
	    System.exit(1);
	}
    }
}