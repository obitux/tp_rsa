#!/bin/bash
#usage ./process.sh diallingCodes ipRanges

infile1=$1
infile2=$2

# process the diallingCodes

outfile=${infile1%.csv}.srt
tmpfile1=${infile1%.csv}.xxx
tmpfile2=${infile1%.csv}.yyy

# remove the first line

sed '1,1d' ${infile1} > ${tmpfile2}

# count the number of lines in the file

count=$(wc -l ${tmpfile2})

# strip the file identifier from the output of the wc command

count=${count%${tmpfile2}}

echo $count > ${tmpfile1}

cat ${tmpfile1} ${tmpfile2} > ${outfile}

rm -f ${tmpfile1} ${tmpfile2}

# process the ipRanges

outfile=${infile2%.csv}.srt
tmpfile1=${infile2%.csv}.xxx
tmpfile2=${infile2%.csv}.yyy

# remove the first line

sed '1,1d' ${infile2} > ${tmpfile1}

# sort: field separator "," sort key is field 5

sort -t , -k 5 ${tmpfile1} > ${outfile}

# remove " characters

sed 's/\"//g' < ${outfile} > ${tmpfile2}

# count the number of lines in the file

count=$(wc -l ${tmpfile2})

# strip the file identifier from the output of the wc command

count=${count%${tmpfile2}}

echo $count > ${tmpfile1}

cat ${tmpfile1} ${tmpfile2} > ${outfile}

rm -f ${tmpfile1} ${tmpfile2}

