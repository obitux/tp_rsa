public class BoysAndGirls
{
    public static void main(String[] args)
    {
	int tries = 0;
	int simulations = 42;
	
	for (int i=0; i<simulations; i++)
	    { 
		int girls = 0;
		int boys = 0;
		
		while (girls==0 || boys==0)
		    {
			double sex = Math.random();
			if (sex < 0.5)
			    girls ++;
			else
			    boys ++;
		    }

		int total = boys + girls;
		tries += total;
		System.out.println("Boys : " + boys + " | Girls : " + girls + " | Total : " + total);
	    }
	
	double average = tries/simulations;
	System.out.println("Average : " + average + " chidrens in " + simulations + " simulations.");
    }
}
