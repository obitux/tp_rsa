public class Tosses
{
    public enum Coin { Head, Tail }

    public static void main (String[] args)
    {
	int alice = 0;
	int bob = 0;
	int n = 20000;

	for (int i=0; i<n; i++)
	    {
		if (aliceTosses()<bobTosses())
		    alice++;
		else
		    bob++;
	    }
	
	double ratio = (double)alice/n;
	System.out.println("Alice: " + alice + " | Bob : " + bob + " | Ratio : " + ratio);
    }

    public static Coin tosse ()
    {
	double result = Math.random();
	if (result<0.5)
	    return Coin.Head;
	else
	    return Coin.Tail;
    }
    
    public static int aliceTosses ()
    {
	Coin a = tosse();
	Coin b = tosse();
	int tries = 2;

	while (a!=Coin.Head || b!=Coin.Head)
	    {
		a = b;
		b = tosse();
		tries++;
	    }

	return tries;
    }

    public static int bobTosses ()
    {
	Coin a = tosse();
	Coin b = tosse();
	int tries = 2;

	while (a!=Coin.Head || b!=Coin.Tail)
            {
		a = b;
		b = tosse();
		tries++;
            }
	
	return tries;
    }
}