public class AvgRand
{
    public static void main(String[] args)
    {
	int n = Integer.parseInt(args[0]);
	double total = 0.;
	
	for (int i=0; i<n; i++)
	    {
		double r = Math.random();
		System.out.printf("%.2g \n", r);
		total += r;
	    }
	
	System.out.printf("Average : %.2g \n", total/n);
    }
}
