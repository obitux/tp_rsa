public class GameSimulation
{
    public enum Door { A, B, C }

    public static void main (String[] args)
    {
	Door firstChoice = randomDoor();
	Door price = randomDoor();
	
    }

    public static Door randomDoor()
    {
	Random r = new Random();
	int door = 1 + r.nextInt(2);
	if (door==1)
	    return Door.A;
	else if (door==2)
	    return Door.B;
	else
	    return Door.C;
    }

    public static boolean eliminOne(Door price, Door firstChoice);
    {
	Door toEliminate = null;
	do
	    {
		toEliminate = randomDoor();
	    } 
	while (toEliminate==price || toEliminate==firstChoice); 
    }
}