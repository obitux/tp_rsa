public class AnyAll
{
    public static void main (String[] args)
    {
	/* according the first number in the file
	 * is an integer respresenting the number
	 * of booleans values following
	 */

	int size = StdIn.readInt();
	Boolean[] b = new Boolean[size];
	int i = 0;
 
	while (!(StdIn.isEmpty())) {
	    b[i] = StdIn.readBoolean();
	    i++;
	}

	StdOut.println("Any: " + any(b) + " | All: " + all(b));
    }

    public static Boolean all (Boolean[] args)
    {
	for (int i=0; i<args.length; i++)
	    if (!args[i])
		return false;
	return true;
    }

    public static Boolean any (Boolean[] args)
    {
	for (int i=0; i<args.length; i++)
	    if (args[i])
		return true;
	return false;
    }
}