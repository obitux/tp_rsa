public class Centroid
{
    public static void main (String[] args)
    {
	int n = Integer.parseInt(args[0]);
	double[] m = new double[n];
	double[] x = new double[n];
	double[] y = new double[n];
	double M = 0.;
	double X = 0.;
	double Y = 0.;
	
	// User input
	for (int i=0; i<n; i++) {
	    StdOut.println("Object number " + (i+1) + " :");
	    StdOut.print("\tMass : ");
	    m[i] = StdIn.readDouble();
	    StdOut.print("\tX : ");
	    x[i] = StdIn.readDouble();
	    StdOut.print("\tY : ");
	    y[i] = StdIn.readDouble();
	}

	// Centroid mass
        for (int i=0; i<n; i++) {
            M += m[i];
	}

	// Centroid X & Y
        for (int i=0; i<n; i++) {
            X += m[i]*x[i]/M;
	    Y += m[i]*y[i]/M;
	}

	StdOut.println("The centroid (x,y,m) is (" + X + " , " + Y + " , " + M + ")");
    }
}