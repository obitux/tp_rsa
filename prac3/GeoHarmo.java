public class GeoHarmo
{
    public static void main (String[] args)
    {
	int N = Integer.parseInt(args[0]);
	double[] list = new double[N];
	
	for (int i=0; i<N; i++) {
	    StdOut.print("Value " + (i+1) + " : ");
	    list[i] = StdIn.readDouble();
	}

	
	System.out.println("Geometric mean : " + geometricMean(list, N));
	System.out.println("Harmonic mean : " + harmonicMean(list, N));
    }

    public static double geometricMean (double[] list, int N)
    {
	double result = 1.0;
	for (int i=0; i<N; i++) {
	    result *= list[i];
	}
	return Math.pow(result, 1/(double)N);
    }
    
    public static double harmonicMean (double[] list, int N)
    {
	double result = 0.0;
	for (int i=0; i<N; i++) {
	    result += 1.0/list[i];
	}
	return result;
    }
}