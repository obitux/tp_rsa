public class Peaks
{
    public static void main (String[] args)
    {
	int weight = StdIn.readInt();
	int height = StdIn.readInt();
	double map[][] = new double[weight][height];
	
	for (int y=0; y<height; y++) {
	    for (int x=0; x<weight; x++) {
		map[x][y] = StdIn.readDouble();
	    }
	}
	
	StdOut.println("peaks: " + countPeaks(weight, height, map));
    }

    public static Boolean isPeak (int x, int y, double[][] map)
    {
	double here = map[x][y];
	return (map[x][y-1] < here &&
		    map[x][y+1] < here &&
		    map[x+1][y] < here &&
		    map[x-1][y] < here);
	}

    public static int countPeaks (int weight, int height, double[][] map)
    {
	int cnt = 0;
	for (int y=1; y<height-1; y++) {
            for (int x=1; x<weight-1; x++) {
		if (isPeak(x, y, map))
		    cnt++;
	    }
	}
	return cnt;
    }
}