public class EuclidianPoint
{
    private double x, y, z;

    public EuclidianPoint (double x, double y, double z) {
	this.x = x;
	this.y = y;
	this.z = z;
    }

    public EuclidianPoint (double x, double y) {
	this.x = x;
	this.y = y;
	this.z = 0;
    }
    
    public EuclidianPoint (double x) {
	this.x = x;
	this.y = 0;
	this.z = 0;
    }

    public double getX () {
	return x;
    }

    public double getY () {
	return y;
    }

    public double getZ () {
	return z;
    }

    public double distanceTo (EuclidianPoint q) {
	return Math.sqrt( Math.pow(x - q.getX() , 2) + 
			  Math.pow(y - q.getY() , 2) + 
			  Math.pow(z - q.getZ() , 2) );
    }

    public static void main (String[] args) {
	EuclidianPoint p = new EuclidianPoint(1,2,3);
	EuclidianPoint q = new EuclidianPoint(4,5,6);
	System.out.println(p.distanceTo(q));
    }
}