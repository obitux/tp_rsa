public class Qubit
{
    private double a, b;

    public Qubit (double a, double b)
    {
	this.a = a;
	this.b = b;
    }

    public Boolean Observe ()
    {
	return Math.pow(a,2) > Math.pow(b,2);
    }
}