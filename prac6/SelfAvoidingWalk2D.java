public class SelfAvoidingWalk2D
{
    public static void main (String[] args) {
	int N = Integer.parseInt(args[0]);
	int T = Integer.parseInt(args[1]);
	int graph = Integer.parseInt(args[2]);
	int deadEnds = 0;
	int escapes = 0;
	int steps = 0;
	int areas = 0;

	// we repeat experience T times
	for (int t=0; t<T; t++) {
	    RandomWalker2D rw = new RandomWalker2D(N);

	    if (graph==1)
		StdDraw.clear(StdDraw.GREEN);

	    // while he isn't in dead end or escaped, he tries to move
	    while (true) {
		if (rw.deadEnd()) {
		    deadEnds ++;
		    break;
		}
		if (!rw.onGrid()) {
		    escapes ++;
		    break;
		}
		rw.step(graph);
	    }

	    steps += rw.steps();
	    areas += rw.area();
	}

	System.out.println(T + " simulations done in " + N + "x" + N + " grids!");
	System.out.println("+ Dead ends : \t\t" + 100*deadEnds/T + "%");
	System.out.println("+ Escaped : \t\t" + 100*escapes/T + "%");
	System.out.println("+ Steps average : \t" + steps/T);
	System.out.println("+ Areas average : \t" + areas/T);
    }
}