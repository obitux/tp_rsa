public class RandomWalker2D
{
    private int N, x, y, steps;
    private boolean[][] grid;
    private int maxX, minX, maxY, minY;
    
    public RandomWalker2D (int N) {
	this.N = N;
	grid = new boolean[N][N];
	x = N/2;
	y = N/2;
	steps = 0;
	maxY = 0;
	maxX = 0;
	minY = 0;
	minX = 0;
    }

    public void step (int graph) {
	if (graph==1) {
	    StdDraw.setXscale(-N/2,+N/2);
	    StdDraw.setYscale(-N/2,+N/2);
	    StdDraw.setPenColor(StdDraw.WHITE);
	    StdDraw.filledSquare(x-N/2,y-N/2,0.45);

	}

	grid[x][y] = true;
	
	// recording max and min coordinates
	if (x>maxX)
	    maxX = x;
	else if (x<minX)
	    minX = x;
	if (y>maxY)
	    maxY = y;
	else if (y<minY)
	    minY = y;

	double r = Math.random();
	
	// if we never gone there we move, else we don't count the step and stay at the same place
	if (r < 0.25) {
	    if (!isTrue(x+1,y)) {
		x++;
		steps ++;
	    }
	}
	else if (r < 0.5) {
	    if (!isTrue(x-1,y)) {
		x--;
		steps ++;
	    }
	}
	else if (r < 0.75) {
	    if (!isTrue(x,y+1)) {
		y++;
		steps++;
	    }
	}
	else if (r < 1.0) {
	    if (!isTrue(x,y-1)) {
		y--;
		steps++;
	    }
	}

	if (graph==1) {
	    StdDraw.setPenColor(StdDraw.BLUE);
	    StdDraw.filledSquare(x-N/2,y-N/2,0.45);
	    StdDraw.show(40);
	}
    }

    // I didn't understood why is this function usefull...
    public double distance() {
	return Math.sqrt(Math.pow((N/2-x),2) + Math.pow((N/2-y),2));
    }

    public boolean deadEnd() {
	return (isTrue(x-1,y) && isTrue(x+1,y) && isTrue(x,y-1) && isTrue(x,y+1));
    }

    public boolean onGrid() {
	return (x>=0 && x<N && y>=0 && y<N);
    }

    private boolean onGrid(int x, int y) {
	return (x>=0 && x<N && y>=0 && y<N);
    }

    // if the next place isn't in the grid, it returns that he never goes there before
    // in order to prevent an out of bound exception when he escapes
    private boolean isTrue(int x, int y) {
	if (onGrid(x, y))
	    return grid[x][y];
	else
	    return false;
    }
    
    // just an accessor
    public int steps() {
	return steps;
    }

    // return the current area of current path
    public int area() {
	return (maxY - minY) * (maxX - minX);
    }
}